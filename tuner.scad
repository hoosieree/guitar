    $fn = 30;
    
    stock = 3.17;
    e = 1e-1;
    
module cradle() {
    difference(){
        cut = stock+e;
        cube([stock,25.4,60], center=true);
        
        // upper slot cutout
        for(i=[-1,1]) {
            translate([0,i*3,10])
            rotate([0,90,0])
            cylinder(h=stock+e,d=6,center=true);
        }
        
        // main cutaways
        translate([0,0,10])
        cube([cut, 6, 6], center=true);
        translate([0,8,-14])
        cube([cut, 8, 30], center=true);
        translate([0,0,-20])
        cube([cut, 5, 10], center=true);
        
        // lower post cutout
        translate([0,4,-2])
        rotate([0,90,0])
        cylinder(h=cut, d=6, center=true);
    }
}

cradle();
