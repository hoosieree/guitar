% Created 2024-09-30 Mon 10:37
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[newfloat]{minted}
\usepackage[margin=1.2in]{geometry}
\usepackage{nopageno}
\usepackage{subfig}
\date{}
\title{g8\\\medskip
\large An 8-string guitar with questionable design choices.}
\hypersetup{
 pdfauthor={ashroyer-admin},
 pdftitle={g8},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 28.2 (Org mode 9.7)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Introduction}
\label{sec:org4e8b2b4}
Reading ``The Anarchist's Workbench''\footnote{\url{https://lostartpress.com/products/the-anarchists-workbench}} inspired me to try woodworking.
The thing I most want to build is a guitar, but I already have both an electric and acoustic guitar so I don't want a copy of either of those because I can only play (at most) one guitar at a time.
Some of the music I like employs extended-range guitars with 7 or 8 strings (typical guitars have 6).
The extra strings are thicker so the instrument can play lower pitch notes, covering some of the range of a bass guitar.

So now I know what I want to build, but \emph{how} should I build it?
I have limited shop space and an assortment of tools, so one constraint is I won't buy any large new tools.
Guitar building is a mature craft and it's possible to buy many of the components pre-made, from fretwire to pickups to fretted necks where the only remaining steps are to finish and set up the instrument.
What interests me most about this is learning what parts I can do myself and what parts I would prefer to pay someone else to do, so at least at first I will try to build as much of it myself as I can.
\section*{Guitar Body}
\label{sec:orgc57e5a6}
\begin{wrapfigure}{r}{0.5\textwidth}
\centering
\includegraphics[width=0.5\textwidth]{./glue-up.png}
\caption{Pine body panel glue-up}
\end{wrapfigure}

Shortly after moving to my current home, it became necessary to cut down a large ash tree which had extensive emerald ash borer\footnote{\url{http://www.emeraldashborer.info/}} damage.
This tree was majestic and it made me sad that it had to die.
The lesson to help prevent spreading invasive species: don't move wood.
I kept some large cuts from near the base of the trunk, and with some help from the guys at work cut them into boards and planed them to uniform thickness.
I glued them together without waiting long enough for them to fully dry, so I ended up with a twisted and glue-split panel approximately the size of a guitar body.
One thing I learned from this experience is that air-drying wood requires a moderate amount of preparation (seal the end grain with something like wax so it dries more uniformly across its length) which I did not do.

Another thing I learned is that you should let wood dry before gluing it together into panels.
For now I've set aside the ash and am working mostly with pine.
The body is two pieces of southern yellow pine from a big box hardware store that's been air-drying for over a year.
Since I do not (yet) have a vise for my workbench, and none of my clamps are large enough, I built a pair of clamps from some scrap wood and wedged them together to keep the pine boards tight while the glue dried.
After it was dry I planed it flat and started making a guitar body from it.

\begin{figure}\centering

\subfloat[Planed Flat]{\label{fig:plane-flat}
\begin{center}
\includegraphics[width=0.25\textwidth]{./body-plane-flat.png}
\end{center}
}\subfloat[Outline Sketch]{\label{fig:drawn-outline}
\begin{center}
\includegraphics[width=0.25\textwidth]{./body-drawn-outline.png}
\end{center}
}\subfloat[Rough Cut]{\label{fig:rough-cut}
\begin{center}
\includegraphics[width=0.25\textwidth]{./body-rough-cut.png}
\end{center}
}\subfloat[Initial Carve]{\label{fig:carved}
\begin{center}
\includegraphics[width=0.25\textwidth]{./body-carved.png}
\end{center}
}\caption{Guitar Body Progress}\label{fig:body-multifig}

\end{figure}
\section*{Electronics}
\label{sec:orgedd0101}
\begin{figure}[htbp]
\centering
\includegraphics[width=0.6\textwidth]{./pup.png}
\caption{Pickup Rendering (Assembled)}
\end{figure}

For a long time I've thought about and sketched designs for a low impedance guitar pickup.
The main inspiration for the design comes from a forum thread\footnote{\url{https://music-electronics-forum.com/forum/instrumentation/pickup-makers/5622-low-impedance-pickup-research}} started by Joseph J. Rogowski back in 2008, and indirectly from the Alumitone pickups made by a company called Lace Music Products\footnote{\url{https://lacemusic.com/products/alumitone-humbucker}}.
Plus a bit of inspiration from Les Paul's balanced pickups in his ``recording'' model electric guitar design\footnote{\url{https://www.vintageguitarandbass.com/gibson/Les\_Paul\_Recording.php}}.
The essential idea is to use a single turn of low-resistance metal as a sensing coil, plus a transformer to increase the output impedance and voltage to reasonable levels for modern recording equipment and guitar electronics.
\subsection*{Design}
\label{sec:orga19b400}
This pickup design prioritizes ease of manufacture with a limited set of tools.
My garage workshop is tight on space and so I will try to make do with hand tools and a drill.
The pickup primary coil is 1/8 inch thick aluminum plate from the local hardware store.
Magnets\footnote{\url{https://www.magnetsource.com/}} induce a magnetic field on steel strings near the primary coil.
A current transformer (either balanced\footnote{\url{https://www.digikey.com/en/products/detail/talema-group-llc/AC1005/3881332?s=N4IgTCBcDaIIxgJwFYC0c4AY6oHIBEQBdAXyA}} or unbalanced\footnote{\url{https://www.digikey.com/en/products/detail/talema-group-llc/AP-1000/4289480?s=N4IgTCBcDaIIxgJwFYC0c4A4DMqByAIiALoC\%2BQA}}) passively transforms the low-voltage/high-current of the primary loop to a low-current/high-voltage output.

\begin{figure}\centering
\subfloat[Exploded Pickup]{\label{fig:exploded}
\begin{center}
\includegraphics[width=0.3\textwidth]{./pup-exploded.png}
\end{center}
}\subfloat[Top View]{\label{fig:top-view}
\begin{center}
\includegraphics[width=0.3\textwidth]{./top-all.png}
\end{center}
}\subfloat[Side View]{\label{fig:side-view}
\begin{center}
\includegraphics[width=0.3\textwidth]{./side-all.png}
\end{center}
}\caption{Pickup Exploded View}\label{fig:multifig}
\end{figure}

The most difficult component for me to fabricate is the large shoulder rivet.
You can make this from 3/8 inch aluminum rod stock, with a shoulder cut on each end and a length of 1/4 inch rivet exposed at each end.
My current approach is to first sand the 3/8`` rod to fit the 11.12 mm inner diameter in the AC1005 transformer, then shape the 1/4'' rivets on each end.
An ideal tool for this job is a metal lathe, but since the only rotating tool I have is a hand drill, I will make a temporary lathe jig to hold the drill in place with the 3/8`` rod in the chuck.
Then I'll use metal files to grind down the ends to shape the shoulder and exposed rivet.

The reason for this shoulder is to take pressure forces when joining the aluminum plates with the aluminum rod rivets.
If there was no shoulder the current transformer might have to bear the force of hammer blows and shatter (not ideal).
The other two rivets are plain 1/4`` aluminum rod stock.
Cut all rivets a bit longer than the assembled height, pound them in with a hammer and anvil, then file smooth.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{./rivets.png}
\caption{Rivets and Current Transformer}
\end{figure}

The Talema AC1005\footnote{\url{https://talema.com/wp-content/uploads/datasheets/AC.pdf}} has a center tap, which makes it possible to provide a balanced output.
I wouldn't want to use an electric guitar without a volume control, which poses a challenge.
Generally, guitars have high output impedance, and use potentiometer wired as a voltage divider to attenuate the output.
However, one of my favorite ways to use a guitar's volume control is as a gain control for a fuzz or distortion.
Fuzz circuits like the classic Fuzz Face have low input impedance, so a ``reversed'' voltage divider inside the guitar doubles as an impedance adjustment going into the fuzz.
For a low-impedance pickup going directly into a low-input-impedance fuzz effect, this reverse voltage divider configuration makes more sense.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.6\textwidth]{./reverse-divider.png}
\caption{Reversed Voltage Divider}
\end{figure}

Connecting straight from guitar to fuzz is the way I prefer to use a fuzz, but most fuzz circuits do not have balanced inputs.
So I'll ignore the balanced output capability of the transformer for now.
The center tap on the Talema AC1005 would in theory also allow for series/parallel or coil-cut features.
These features are useful on normal high-impedance guitar pickups because of the interesting frequency responses of the coils.
With a low-impedance coil I doubt that series/parallel or coil tapping would accomplish anything other than a volume reduction, so I plan to not use that feature either.
\subsection*{Prototype}
\label{sec:org087aa20}
I wish I had a lathe.
The shoulder rivet would be easy to turn on a real lathe.
But I made do with a horrible imitation of a lathe, an abomination really.

\begin{figure}\centering
\subfloat[Cursed Lathe]{\label{fig:cursed-lathe}
\begin{center}
\includegraphics[width=0.5\textwidth]{./lathe.png}
\end{center}
}\subfloat[Top View]{\label{fig:rivet-proto}
\begin{center}
\includegraphics[width=0.5\textwidth]{./rivet-proto.png}
\end{center}
}\caption{Prototype Rivet}\label{fig:rivet-multifig}
\end{figure}

Yes, that's an electric drill clamped to some bits of scrap wood.
Yes, I used a Dremel cutoff wheel attachment as the cutter.
No, the results are not impressive.

But there are results!

I cut the protruding nubs a little too small, so I eventually remade this shoulder rivet.
Then I cut the top and bottom plates and did a test fit.
This test told me I need a drill press or some other way to drill straight holes (maybe a jig is in my future).

\begin{figure}\centering
\subfloat[Front]{\label{fig:exploded}
\begin{center}
\includegraphics[width=0.3\textwidth]{./assemble-1.png}
\end{center}
}\subfloat[Short Side]{\label{fig:top-view}
\begin{center}
\includegraphics[width=0.3\textwidth]{./assemble-2.png}
\end{center}
}\subfloat[Long Side]{\label{fig:side-view}
\begin{center}
\includegraphics[width=0.3\textwidth]{./assemble-3.png}
\end{center}
}\caption{Pickup Assembly Before Magnets}\label{fig:multifig}
\end{figure}

The important thing is that all the parts fit together and I can move on to adding magnetics and wiring the output transformer leads to a 1/4`` jack for testing.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{./pup-assembled-bottom.jpg}
\caption{Magnets Added}
\end{figure}
\subsection*{Next Steps}
\label{sec:org23f94c1}
This project inspired me to add to my shopping list:
\begin{itemize}
\item tapered reamer
\item bench vise or better yet a more functional workbench
\item drill press or at least some way to drill precise and straight holes
\item better drill
\item anvil
\end{itemize}
\section*{Strings}
\label{sec:orgc3150fd}
The strings are Stringjoy ``Signatures'' nickel wound.
I discovered Stringjoy while experimenting with custom gauges for an extended tuning range experiment.
Their calculator\footnote{\url{https://tension.stringjoy.com/}} lets you predict the tension of a string tuned to a particular pitch with a given scale length.
Surprisingly, almost no string companies actually make this information easily accessible, so Stringjoy's calculator stood out.
An even bigger surprise was that the price for a custom set of strings was the same as a normal set.
The strings themselves are as good as any I've used from bigger name brands, so I'm now a loyal customer.

\begin{center}
\begin{tabular}{rrllr}
length (mm) & length (in.) & string & pitch & \href{https://tension.stringjoy.com/}{tension} (lbs.)\\
\hline
647.7 & 25.50 & 0.010 & E4 & 17.8\\
656.7302271002411 & 25.85 & 0.0135 & B3 & 18.7\\
665.7751222500947 & 26.21 & 0.17 & G3 & 19.3\\
674.8340956560974 & 26.56 & 0.026w & D3 & 21.7\\
683.9065878800241 & 26.92 & 0.034w & A2 & 21.1\\
692.9920679663451 & 27.28 & 0.046w & E2 & 21\\
702.090031702278 & 27.64 & 0.060w & B1 & 20.7\\
711.2 & 28.00 & 0.080w & E1 & 16\\
\end{tabular}
\end{center}
\section*{Neck}
\label{sec:org18af9a8}
\begin{figure}[htbp]
\centering
\includegraphics[width=0.9\textwidth]{./plan.jpg}
\caption{In Progress}
\end{figure}

\begin{itemize}
\item maple and/or mahogany laminate from Menard's, TBD
\item multi-scale (647mm to 711mm) (25.5in to 28in)
\item perpendicular at 1/2
\item interactive model linked in Appendix: FretFind2D
\end{itemize}

\begin{figure}[htbp]
\centering
\includegraphics[angle=90,width=1.0\textwidth]{./fretboard_1.pdf}
\caption{Fret Layout and String Spacing}
\end{figure}
\section*{Tuners}
\label{sec:orge558d63}
In keeping with this guitar build's theme of over-complicating everything, rather than purchase ready-made tuners I thought, ``what can I make?''
My plan is to build a set of tuners inspired by Steinberger's \href{https://patents.google.com/patent/US9564110B2}{US9564110} double-clamping design.
Hopefully I can figure out the geometry that will provide enough clamping pressure for the small strings while still being compact enough to fit 8 behind the bridge with room for tool-free adjustments.
Started sketching out a design in TinkerCAD.
\begin{figure}[htbp]
\centering
\includegraphics[width=0.6\textwidth]{./tuner0.png}
\caption{Tuner CAD Sketch}
\end{figure}
\section*{String spacing at nut:}
\label{sec:org27cf3bd}
\begin{itemize}
\item center offsets: 0 7.30 14.69 22.23 30.02 38.09 46.46 55.24
\item s1-s8 distance: 55.24 (2.175 in.)
\item nut width:      63.24 (2.490 in.)
\end{itemize}
\section*{String spacing at bridge:}
\label{sec:orgb95271d}
\begin{itemize}
\item center offsets: 0 10.30 20.69 31.23 42.02 53.09 64.46 76.24
\item s1-s8 distance: 76.24 (3.001 in.)
\item bridge width:   84.24 (3.316 in.)
\end{itemize}
\section*{Appendix: SCAD Pickup Model}
\label{sec:orgbb11b0a}
\begin{verbatim}
// units: mm
$fn = 10; //larger number for more fine radii
centers = [0,10.30,20.69,31.23,42.02,53.09,64.46,76.24]; //string centers
dmag = mm(1/4)+1;
drivet = mm(3/8);
r1 = mm(3/16); // small rivet
e = 0.02; //epsilon
width = 84.24; //bridge width
length = mm(2); //2 inch wide plate
thickness = 3.175;
border = 30;

function mm(inches) = inches * 25.4;

module magnet_cuts(){
 for(j=[-1,1]){
  for(i=centers){
   translate([-width/2+dmag/2+i,j*length/4,0])
    // cylinder(h=thickness+e,d=dmag,center=true);
    rotate(45) cube([3,3,thickness+e],center=true); //diamonds for alignment
  }
  translate([centers[1]*1,j*length/4,0])
   cube([width+centers[1],1,thickness+e],center=true);
 }
}

module rivet_holes(h){
 for(i=[-1,0,1]){
  translate([width*0.56,i*(length/3+3),h])
   cylinder(h=thickness+e,d=r1,center=true);
 }
}

module upper(){
 difference(){
  cube([width+border,length,thickness],center=true);
  magnet_cuts();
  rivet_holes(0);
 }
}

module lower(spacing){
 difference(){
  translate([width-border-7,0,spacing])
   cube([20,length,thickness],center=true);
  rivet_holes(spacing);
 }
}

module ac1005(){
 d1 = 23.8;
 d2 = 9.8;
 d3 = (d1-15.24);
 h = 11.12;
 wl = 8;
 difference(){
  union(){
   cylinder(d=d1,h=h,center=true);
   translate([0,d1/4,0]) cube([d1,d1/2,h],center=true);
   translate([0,15,h/2-1.75]) rotate([90,0,0]) cylinder(d=1,h=wl,center=true);
   translate([0,d1/2,h/2-1.75]) rotate([90,0,0]) cylinder(d=3,h=1,center=true);
   translate([d3,15,-h/2+1.75]) rotate([90,0,0]) cylinder(d=1,h=wl,center=true);
   translate([-d3,15,-h/2+1.75]) rotate([90,0,0]) cylinder(d=1,h=wl,center=true);
   translate([d3-1,d1/2,0]) cylinder(d=1,h=h,center=true);
   translate([-d3+1,d1/2,0]) cylinder(d=1,h=h,center=true);
  }
  cylinder(d=d2,h=h+e,center=true);
 }
}

module rivet(h){
 translate([width/2+5,0,h])
  union(){
  cylinder(d=r1,h=mm(1/4)+12,center=true);
  cylinder(d=drivet,h=12,center=true);
 }
 for(i=[-1,1]){
  translate([width/2+5,i*(length/3+3),h])
   cylinder(d=r1,h=mm(1/4)+12,center=true);
 }
}

module main(explode,plates){
 h2 = explode ? -30 : -mm(1/4)-1;
 h1 = explode ? -40 : h2*2;
 h3 = explode ? -13 : -7.4;
 union(){
  upper();
  translate([plates?21:0,0,0]) lower(h1); //for flat display
  // translate([plates?38:0,0,0]) rivet(h3);
  // translate([plates?64:0,0,0])
  //  translate([width-border-7,0,h2])
  //   rotate(90) ac1005();
 }
}
projection()
translate([-width/2,-length,0]) rotate(90) main(false,true);
\end{verbatim}
\section*{Appendix: FretFind2D}
\label{sec:org60d7f7f}
\url{https://acspike.github.io/FretFind2D/src/fretfind.html\#showFretboardEdges=on\&showStrings=on\&extendFrets=on\&showBoundingBox=on\&showMetas=on\&len=25\&lenF=647.7\&lenL=711.2\&pDist=0.5\&ipDist=0.5\&nutWidth=55.24\&bridgeWidth=76.24\&oE=5\&oN=4\&oB=6\&oL=0.09375\&oF=0.09375\&oNL=0.09375\&oNF=0.09375\&oBL=0.09375\&oBF=0.09375\&root=12\&scl=!+12tet.scl\%0A!\%0A12+tone+equal+temperament\%0A12\%0A!\%0A100.0\%0A200.\%0A300.\%0A400.\%0A500.\%0A600.\%0A700.\%0A800.\%0A900.\%0A1000.\%0A1100.\%0A2\%2F1\%0A\&numFrets=24\&numStrings=8\&t\%5B\%5D=0\&t\%5B\%5D=0\&t\%5B\%5D=0\&t\%5B\%5D=0\&t\%5B\%5D=0\&t\%5B\%5D=0\&t\%5B\%5D=0\&t\%5B\%5D=0\&il\%5B\%5D=25\&il\%5B\%5D=25.5\&il\%5B\%5D=26\&il\%5B\%5D=26.5\&il\%5B\%5D=27\&il\%5B\%5D=27.5\&il\%5B\%5D=28\&il\%5B\%5D=28.5\&ig\%5B\%5D=0\&ig\%5B\%5D=0\&ig\%5B\%5D=0\&ig\%5B\%5D=0\&ig\%5B\%5D=0\&ig\%5B\%5D=0\&ig\%5B\%5D=0\&ig\%5B\%5D=0\&u=mm\&sl=multiple\&ns=equal\&scale=et\&o=nutbridge}
\end{document}
