// units: mm
$fn = 10; //larger number for more fine radii
centers = [0,10.30,20.69,31.23,42.02,53.09,64.46,76.24]; //string centers
dmag = mm(1/4)+1;
drivet = mm(3/8);
r1 = mm(3/16); // small rivet
e = 0.02; //epsilon
width = 84.24; //bridge width
length = mm(2); //2 inch wide plate
thickness = 3.175;
border = 30;

function mm(inches) = inches * 25.4;

module magnet_cuts(){
 for(j=[-1,1]){
  for(i=centers){
   translate([-width/2+dmag/2+i,j*length/4,0])
    // cylinder(h=thickness+e,d=dmag,center=true);
    rotate(45) cube([3,3,thickness+e],center=true); //diamonds for alignment
  }
  translate([centers[1]*1,j*length/4,0])
   cube([width+centers[1],1,thickness+e],center=true);
 }
}

module rivet_holes(h){
 for(i=[-1,0,1]){
  translate([width*0.56,i*(length/3+3),h])
   cylinder(h=thickness+e,d=r1,center=true);
 }
}

module upper(){
 difference(){
  cube([width+border,length,thickness],center=true);
  magnet_cuts();
  rivet_holes(0);
 }
}

module lower(spacing){
 difference(){
  translate([width-border-7,0,spacing])
   cube([20,length,thickness],center=true);
  rivet_holes(spacing);
 }
}

module ac1005(){
 d1 = 23.8;
 d2 = 9.8;
 d3 = (d1-15.24);
 h = 11.12;
 wl = 8;
 difference(){
  union(){
   cylinder(d=d1,h=h,center=true);
   translate([0,d1/4,0]) cube([d1,d1/2,h],center=true);
   translate([0,15,h/2-1.75]) rotate([90,0,0]) cylinder(d=1,h=wl,center=true);
   translate([0,d1/2,h/2-1.75]) rotate([90,0,0]) cylinder(d=3,h=1,center=true);
   translate([d3,15,-h/2+1.75]) rotate([90,0,0]) cylinder(d=1,h=wl,center=true);
   translate([-d3,15,-h/2+1.75]) rotate([90,0,0]) cylinder(d=1,h=wl,center=true);
   translate([d3-1,d1/2,0]) cylinder(d=1,h=h,center=true);
   translate([-d3+1,d1/2,0]) cylinder(d=1,h=h,center=true);
  }
  cylinder(d=d2,h=h+e,center=true);
 }
}

module rivet(h){
 translate([width/2+5,0,h])
  union(){
  cylinder(d=r1,h=mm(1/4)+12,center=true);
  cylinder(d=drivet,h=12,center=true);
 }
 for(i=[-1,1]){
  translate([width/2+5,i*(length/3+3),h])
   cylinder(d=r1,h=mm(1/4)+12,center=true);
 }
}

module main(explode,plates){
 h2 = explode ? -30 : -mm(1/4)-1;
 h1 = explode ? -40 : h2*2;
 h3 = explode ? -13 : -7.4;
 union(){
  upper();
//  translate([plates?21:0,0,0]) // uncomment for projection
     lower(h1);
//  translate([plates?38:0,0,0]) // uncomment for projection
     rivet(h3);
//  translate([plates?64:0,0,0]) // uncomment for projection
  translate([width-border-7,0,h2])
  rotate(90) ac1005();
 }
}
//projection()  // uncomment for flat display
translate([-width/2,-length,0]) rotate(90) main(false,true);
